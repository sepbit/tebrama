# Changelog

See [Keep a Changelog](http://keepachangelog.com/).

## [1.1.0] - 2022-03-19
### Changed
- Move to Sistamapy library

## [1.0.0] - 2021-06-12
### Added
- First release!
